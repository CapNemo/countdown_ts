#/bin/bash
SKRIPT_STARTPATH=$PWD;
TARGET_PATH="$PWD/release";
HTML_FILE_PATH="$PWD/app";
JS_SCRIPT_PATH="$PWD/app";
CSS_STYLE_PATH="$PWD/app/style"
if [ -e $TARGET_PATH ]
	then
		:
	else
		echo "INFO: Lege Releaseverzeichnis an"
		mkdir ./release
fi
if [ -e ./webapp.manifest ]
	then
		cp ./webapp.manifest ./release/
	else
		echo "WARNING: NO WEBAPP.MANIFEST FOUND!";
fi
cd $HTML_FILE_PATH
for i in $(ls *.html)
	do
		echo "Optimiere HTML: $i"
		html-minifier --keep-closing-slash --minify-css --minify-js --remove-comments $i > "$TARGET_PATH/$i"
	done
echo "Komprimiere JavaScript sources"
cd $JS_SCRIPT_PATH;
for i in $(ls *.js)
	do
	echo "INFO: Optimiere $i";
	if ccjs "$i" > "$TARGET_PATH/$i" 2> /dev/null --language_in=ECMASCRIPT5
		then
			echo "INFO: $i Optimiert";
		else
			echo "Fehler beim Optimieren von $i"
			cp "$i" "./release/$i";
	fi
	done
cd $SKRIPT_STARTPATH
cd release
if [ -e ./style ]
	then
		:
	else
		echo "INFO: Lege Styleverzeichnis an"
		mkdir ./style
fi
cd $CSS_STYLE_PATH
for i in $(ls *.css)
	do
		echo "Optimiere CSS: $i"
		yuicompressor $i > "$TARGET_PATH/style/$i"
	done
echo "Baue Package"
cd $TARGET_PATH
zip -r ../package/countdown.zip * > /dev/null
echo "FERTIG!"

