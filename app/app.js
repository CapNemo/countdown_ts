var App = (function () {
    function App(locale) {
        var _this = this;
        this.edit = false;
        this.help1 = "";
        this.clearContent = false;
        this.array = new Array();
        this.db = new AsyncDB(App.NAME, "Counters", function () { alert(_this._t("db_fail") || App.MSG_DB_FAIL); }, this.dbToArray.bind(this));
        var doc = document;
        this.body = doc.body;
        this.header = doc.getElementById("header");
        this.button_back = this.header.querySelector("#button_back");
        this.button_add = this.header.querySelector("#button_add");
        this.content = doc.getElementById("content");
        this.editPage = doc.getElementById("editPage");
        this.newDate = doc.getElementById("newDate");
        this.newTime = doc.getElementById("newTime");
        this.newName = doc.getElementById("newName");
        this.wholeDay = doc.getElementById("wholeDay");
        this.showDate = doc.getElementById("showDate");
        if (locale) {
            this._t = locale._t.bind(locale);
        }
        else {
            this._t = function () { console.error("Localization not possible!"); return null; };
        }
    }
    App.prototype.main = function () {
        window.setInterval(this.refreshCounters.bind(this), App.REFRESH_RATE);
    };
    App.prototype.showSelectedDate = function (date) {
        this.showDate.innerHTML = date.toLocaleString();
    };
    App.prototype.connectCallbacks = function () {
        var _this = this;
        this.button_back.addEventListener("click", function () {
            _this.edit = false;
            _this.showMain();
            _this.button_back.style.display = "none";
            _this.button_add.innerHTML = _this.help1;
            _this.button_add.style.display = "inline";
        });
        this.button_add.addEventListener("click", function () {
            if (_this.edit) {
                try {
                    _this.createNewCounter();
                    _this.edit = false;
                    _this.button_add.innerHTML = _this.help1;
                    _this.button_back.style.display = "none";
                }
                catch (e) {
                    alert(e.message);
                }
            }
            else {
                _this.help1 = _this.button_add.innerHTML;
                _this.showAddPage();
                _this.button_add.innerHTML = "OK";
                _this.button_back.style.display = "inline";
                _this.edit = true;
            }
        });
        this.wholeDay.onchange = function (e) {
            if (_this.wholeDay.checked) {
                _this.newTime.disabled = true;
                _this.newTime.classList.add("disabled");
            }
            else {
                _this.newTime.disabled = false;
                _this.newTime.classList.remove("disabled");
            }
        };
    };
    App.prototype.refreshCounters = function () {
        var date = Date.now();
        var length = this.array.length;
        for (var i = 0; i < length; i++) {
            this.array[i].refresh(date);
        }
    };
    /**
    * Liest alle Counter aus der Datenbank in das Array
    */
    App.prototype.dbToArray = function () {
        var _this = this;
        this.array.length = 0;
        this.db.forEach(function (el) { _this.array.push(Counter.toCounter(el)); }, this.buildUI.bind(this));
    };
    App.prototype.deleteCounter = function (counter) {
        this.db.remove(counter.DestName, this.dbToArray.bind(this));
        var parent = counter.target.parentElement;
        var popup = counter.popup;
        parent.parentElement.removeChild(parent);
        popup.parentElement.removeChild(popup);
    };
    App.prototype.updateDB = function (newValue) {
        this.db.add(newValue, newValue.DestName, this.buildUI.bind(this));
    };
    App.prototype.addCounter = function (TargetDate, DestName) {
        var _this = this;
        check(this.array != null, "TargetArray does not exist!");
        this.db.contains(DestName, function (b) {
            if (!b) {
                _this.array.push(new Counter(TargetDate, DestName));
                _this.updateDB(new Counter(TargetDate, DestName).toObject());
            }
            else
                alert(_this._t("msg_not_unique") || App.MSG_NOT_UNIQUE);
        });
    };
    App.prototype.createNewCounter = function () {
        var new_date = this.newDate.value;
        var new_time = this.newTime.value || "00:00"; //DEFAULT TO 0:00
        var name = this.newName.value;
        check(name != null && name.length > 0, this._t("no_name") || App.MSG_NO_NAME);
        check(new_date != null, this._t("no_date") || App.MSG_NO_DATE);
        /* TODO:
        if(! this.full_day.enabled){
            check(new_time != null, this._t("no_time")|| App.MSG_NO_TIME);
        }
        */
        var datetime = new DateTime(new_date, new_time, true);
        var date = datetime.getDate();
        this.addCounter(date, name);
        this.showMain();
    };
    App.prototype.firstStart = function () {
        var title = document.createElement('h1');
        title.innerHTML = this._t("greet") || App.GREETING;
        this.content.appendChild(title);
        this.clearContent = true;
    };
    App.prototype.buildUI = function () {
        var doc = document;
        if (this.array.length == 0) {
            this.firstStart();
        }
        else {
            if (this.clearContent) {
                this.content.innerHTML = "";
                this.clearContent = false;
            }
            var fragment = doc.createDocumentFragment();
            for (var i = 0; i < this.array.length; i++) {
                var el = this.array[i];
                if (!this.exists(el.DestName)) {
                    fragment.appendChild(this.buildDiv(el));
                    fragment.appendChild(el.popup);
                    el.refresh(Date.now());
                }
                else if (!el.target) {
                    el.target = doc.getElementById(el.DestName);
                }
            }
            this.content.appendChild(fragment);
        }
    };
    App.prototype.buildDiv = function (c) {
        var doc = document;
        var title = doc.createElement('h2');
        title.innerHTML = c.DestName;
        var div = doc.createElement('div');
        div.onclick = c.showPopup.bind(c);
        div.appendChild(title);
        var p = doc.createElement('div');
        p.className = "count_target";
        p.id = c.DestName;
        c.target = p;
        var legend = doc.createElement('div');
        legend.className = "legend";
        legend.innerHTML = this._t("legend") || "Days</br>Hours</br>Minutes</br>Seconds";
        div.appendChild(legend);
        div.appendChild(p);
        return div;
    };
    App.prototype.exists = function (id) {
        return (!(!document.getElementById(id)));
    };
    /* Verbirgt die Countdowns und zeigt die Seite zum erstellen neuer */
    App.prototype.showAddPage = function () {
        this.content.style.display = 'none';
        this.editPage.style.display = 'block';
    };
    /* Kehrt zur Hauptseite zurueck */
    App.prototype.showMain = function () {
        this.content.style.display = 'block';
        this.editPage.style.display = 'none';
    };
    //Konstanten
    App.REFRESH_RATE = 1000; /* RefreshRate fuer die Counter */
    App.NAME = "CountdownApp";
    //Nachrichten
    App.MSG_DB_FAIL = "An Error occured while opening the database";
    App.MSG_NO_NAME = "Please enter a name";
    App.MSG_DATE_INVALID = "Invalid date";
    App.MSG_NOT_UNIQUE = "This name is already in use. Please choose another one";
    App.MSG_NO_DATE = "Please set a date first";
    App.GREETING = "It's empty here. </br>Create some countdowns";
    return App;
})();
var Counter = (function () {
    function Counter(TargetDate, DestName, target) {
        this.TargetDate = TargetDate;
        this.DestName = DestName;
        this.target = target;
        this.newPopup();
    }
    Counter.prototype.refresh = function (target_time) {
        var diff = (this.TargetDate.getTime() - target_time) / 1000 << 0;
        var target = this.target;
        if (target == null) {
            //missing Div
            return;
        }
        if (diff > 0) {
            var days = diff / 86400 << 0;
            var rest = diff - days * 86400;
            var hours = rest / 3600 << 0;
            rest -= hours * 3600;
            var minutes = rest / 60 << 0;
            rest -= minutes * 60;
            var seconds = rest << 0;
            days = this.format2(days);
            hours = this.format2(hours);
            minutes = this.format2(minutes);
            seconds = this.format2(seconds);
            var result = days + "</br>" + hours + "</br>" + minutes + "</br>" + seconds;
            target.innerHTML = result;
        }
        else {
            var result = Locale._t("msg_reach") || Counter.MSG_REACH;
            var parent = target.parentElement;
            /*TODO: Is there a better solution? */
            var legend = parent.getElementsByClassName("legend").item(0);
            if (legend)
                parent.removeChild(legend);
            /* END TODO*/
            target.className = "rocket";
            target.innerHTML = result;
            this.refresh = function (e) {
            };
        }
    };
    Counter.toCounter = function (a) {
        if (!(a instanceof Counter)) {
            var count = a;
            return new Counter(count.TargetDate, count.DestName, count.target);
        }
    };
    Counter.prototype.toObject = function () {
        var o = { TargetDate: this.TargetDate, DestName: this.DestName, target: this.target };
        return (o);
    };
    Counter.prototype.showPopup = function () {
        this.popup.className = "popupVisible";
    };
    Counter.prototype.hidePopup = function () {
        this.popup.className = "popupHidden";
    };
    Counter.prototype.newPopup = function () {
        this.popup = document.createElement('div');
        this.popup.className = "popupHidden";
        var result = document.createElement('span');
        result.innerHTML = ("<p>"
            + (Locale._t("name") || "Name:")
            + "	"
            + this.DestName + "</br>"
            + (Locale._t("start") || "Start:")
            + "	"
            + this.TargetDate.toLocaleDateString()
            + "</br>"
            + (Locale._t("at") || "at:")
            + "		"
            + this.TargetDate.toLocaleTimeString()
            + " "
            + (Locale._t("clock") || "") + "</p>");
        var buttonbox = document.createElement('div');
        buttonbox.className = "buttonbox";
        var deleteButton = document.createElement('button');
        var cancelButton = document.createElement('button');
        cancelButton.innerHTML = Locale._t("cancel") || "Cancel";
        deleteButton.className = "danger";
        deleteButton.innerHTML = Locale._t("delete") || "Delete";
        deleteButton.addEventListener('click', this.remove.bind(this));
        cancelButton.addEventListener('click', this.hidePopup.bind(this));
        buttonbox.appendChild(cancelButton);
        buttonbox.appendChild(deleteButton);
        this.popup.appendChild(result);
        this.popup.appendChild(buttonbox);
    };
    Counter.prototype.remove = function () {
        var remove = window.confirm(Locale._t("conf_del") || Counter.CONF_DEL);
        if (remove) {
            app.deleteCounter(this);
        }
    };
    Counter.prototype.format2 = function (n) {
        if (n > -1 && n < 10) {
            return "0" + n;
        }
        else {
            return n;
        }
    };
    Counter.MSG_REACH = "<strong>has been reached</strong>";
    Counter.CONF_DEL = "Delete this countdown ?";
    return Counter;
})();
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var app; //MAIN APPLICATION CLASS
var transl;
function main() {
    transl = Locale.instance();
    start_app();
}
var start_app = function () {
    app = new App(transl);
    app.connectCallbacks();
    app.main();
};
var Exception = (function () {
    function Exception(message) {
        this.message = message;
    }
    Exception.name = "Exception";
    return Exception;
})();
var InvalidInputException = (function (_super) {
    __extends(InvalidInputException, _super);
    function InvalidInputException() {
        _super.apply(this, arguments);
    }
    InvalidInputException.name = "InvalidInputException";
    return InvalidInputException;
})(Exception);
function check(assertion, message) {
    if (!assertion) {
        throw new InvalidInputException(message);
    }
}
/**
 * Checks if a given datestring is valid.
 * They should look like this:
 * YYYY-MM-DD-hh-mm			e.g. 2015-01-01-23-59
 */
var DateInvalidException = (function (_super) {
    __extends(DateInvalidException, _super);
    function DateInvalidException() {
        _super.apply(this, arguments);
    }
    DateInvalidException.name = "DateInvalidException";
    return DateInvalidException;
})(Exception);
var DateTime = (function () {
    function DateTime(dateString, timeString, hasTime) {
        this.setDateString(dateString, timeString, hasTime);
    }
    DateTime.prototype.setDateString = function (dateString, timeString, hasTime) {
        this.hasTime = hasTime;
        if (dateString == null || timeString == null)
            throw new DateInvalidException(DateTime.MSG_NO_DATE);
        var tmp = dateString + "-" + timeString.replace(':', '-');
        this.split = tmp.split("-");
        if (this.isValid()) {
            this.date = new Date(this.split[0], (this.split[1] - 1), this.split[2], this.split[3], this.split[4]);
        }
        else
            throw new DateInvalidException(DateTime.MSG_INVALID_DATE);
    };
    DateTime.prototype.isValid = function () {
        var valid = true;
        valid = (this.split.length == 5 && this.hasTime || this.split.length == 3 && !this.hasTime);
        console.log(valid);
        valid = valid && (this.split[0].length != 0 && this.split[1].length != 0 && this.split[2].length != 0);
        console.log(valid);
        var year = parseInt(this.split[0]);
        var month = parseInt(this.split[1]);
        var day = parseInt(this.split[2]);
        valid = valid && year > 1500 && year <= 9999;
        valid = valid && month <= 12 && month >= 1;
        valid = valid && day <= 31 && day >= 1;
        if (this.hasTime) {
            var hour = parseInt(this.split[3]);
            var minute = parseInt(this.split[4]);
            valid = valid && (hour <= 24 && hour >= 0);
            valid = valid && (minute <= 59 && minute >= 0);
        }
        return valid;
    };
    DateTime.prototype.getDate = function () {
        return this.date;
    };
    DateTime.MSG_NO_DATE = "Date or Time is empty";
    DateTime.MSG_INVALID_DATE = "Invalid Date";
    return DateTime;
})();
var AsyncDatabaseException = (function () {
    function AsyncDatabaseException(message) {
        this.message = message;
    }
    AsyncDatabaseException.name = "asyncDatabaseException";
    return AsyncDatabaseException;
})();
var DBElement = (function () {
    function DBElement(key, value) {
        this.key = key;
        this.value = value;
    }
    return DBElement;
})();
/**
 * AsyncDB is a class to simplify interaction
 * with IndexedDB.
 */
var AsyncDB = (function () {
    function AsyncDB(dbName, storeName, callback_error, callback_success) {
        this.filledKeys = 0;
        this.DB_NAME = "asyncDB";
        this.STORE_NAME = storeName.toUpperCase();
        this.DB_NAME = dbName;
        this.init(callback_error, callback_success);
    }
    /**
     * Initialisiert die IndexedDB und ruft anschliessend callback_success auf
     * oder callback_error im Falle eines Fehlers
     * @param callback_error Funktionsreferenz fuer den Fehlerfall
     * @param callback_success	Aufzurufende Funktion nach erfolgreicher Initialisierung
     */
    AsyncDB.prototype.init = function (callback_error, callback_success) {
        var _this = this;
        var indexedDB = window.webkitIndexedDB || window.mozIndexedDB || window.indexedDB;
        if (!indexedDB) {
            callback_error();
        }
        else {
            var request = indexedDB.open(this.DB_NAME, 21);
            request.onerror = function (event) {
                alert("Database Error " + request.error);
                if (callback_error) {
                    alert(request.error);
                    callback_error();
                }
            };
            request.onsuccess = function () {
                _this.database = request.result;
                if (callback_success) {
                    callback_success();
                }
            };
            request.onupgradeneeded = function (e) {
                _this.database = request.result;
                var objstores = _this.database.objectStoreNames;
                if (objstores.contains(_this.STORE_NAME)) {
                }
                else
                    _this.database.createObjectStore(_this.STORE_NAME, { keyPath: "key" });
                console.log("Store: " + _this.STORE_NAME + " erstellt");
            };
        }
    };
    AsyncDB.prototype.db_to_array = function (array, callback) {
        var trans = this.database.transaction(this.STORE_NAME, 'readonly');
        var store = trans.objectStore(this.STORE_NAME);
        var request = store.openCursor();
        var target = array;
        array.length = 0;
        request.onsuccess = function (e) {
            var result = request.result;
            if (result) {
                var data = result.value.value;
                var key = (result).key;
                target.push(data);
                result.continue();
            }
            else {
            }
            ;
        };
        trans.oncomplete = function () {
            if (callback) {
                callback();
            }
        };
    };
    AsyncDB.prototype.forEach = function (doFunction, callback) {
        var trans = this.database.transaction(this.STORE_NAME, 'readonly');
        var store = trans.objectStore(this.STORE_NAME);
        var request = store.openCursor();
        request.onsuccess = function (e) {
            var result = request.result;
            if (result) {
                var data = result.value.value;
                doFunction(data);
                result.continue();
            }
        };
        trans.oncomplete = callback;
    };
    AsyncDB.prototype.clear = function (callback) {
        var trans = this.database.transaction(this.STORE_NAME, 'readwrite');
        var store = trans.objectStore(this.STORE_NAME);
        store.clear();
        trans.oncomplete = function () {
            if (callback) {
                callback();
            }
        };
    };
    AsyncDB.prototype.add = function (object, key, callback) {
        var _this = this;
        var trans = this.database.transaction(this.STORE_NAME, 'readwrite');
        var store = trans.objectStore(this.STORE_NAME);
        var data = new DBElement(key, object);
        var addRequest = store.add(data);
        addRequest.onsuccess = function () {
            _this.filledKeys++;
        };
        addRequest.onerror = function () {
            console.error("Fehler beim Hinzufügen in die Datenbank");
        };
        trans.oncomplete = function () {
            if (callback) {
                callback();
            }
        };
    };
    AsyncDB.prototype.contains = function (key, callback) {
        var trans = this.database.transaction([this.STORE_NAME], 'readwrite');
        var store = trans.objectStore(this.STORE_NAME);
        var request = store.count(key);
        request.onsuccess = function () {
            callback(!(request.result === 0));
        };
    };
    AsyncDB.prototype.remove = function (key, callback) {
        if (!key) {
            console.error("key is undefined");
            return;
        }
        var trans = this.database.transaction([this.STORE_NAME], 'readwrite');
        var store = trans.objectStore(this.STORE_NAME);
        var request = (store.delete(key));
        trans.oncomplete = callback;
    };
    return AsyncDB;
})();
var Locale = (function () {
    function Locale() {
        this.lang = navigator.language || "en";
        var path = this.lang + ".js";
        this.loadLanguage(path);
    }
    Locale.instance = function () {
        if (Locale.inst) {
            return Locale.inst;
        }
        else
            Locale.inst = new Locale();
        return Locale.inst;
    };
    Locale.prototype.set_data = function (data) {
        this.data = data;
        this.translateByAttribute();
    };
    Locale.prototype.translateByAttribute = function () {
        var nodes = document.querySelectorAll('[lang-prop]');
        for (var i = 0; i < nodes.length; ++i) {
            var el = nodes[i];
            var prop = el.getAttribute("lang-prop");
            if (el.tagName === "INPUT") {
                if (this._t(prop))
                    el.setAttribute("placeholder", this._t(prop));
            }
            else {
                if (this._t(prop))
                    el.innerHTML = this._t(prop);
            }
        }
    };
    Locale.prototype.loadLanguage = function (path) {
        var script = document.createElement("script");
        script.setAttribute('type', "text/javascript");
        script.setAttribute('src', path);
        document.getElementsByTagName('head')[0].appendChild(script);
    };
    /**
     * Returns the translated text to a given property in
     * the users language
     */
    Locale.prototype._t = function (prop) {
        if (this.data) {
            if (this.data[prop])
                return this.data[prop];
        }
        console.error("Language: Property " + prop + " not found!");
        return null;
    };
    Locale._t = function (prop) {
        return Locale.instance()._t(prop);
    };
    return Locale;
})();
