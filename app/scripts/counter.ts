class Counter {
	private static MSG_REACH = "<strong>has been reached</strong>";
	private static CONF_DEL = "Delete this countdown ?";
	public popup: HTMLElement;
	constructor( public TargetDate: Date, public DestName: string, public target?: HTMLElement ) {
		this.newPopup();
	}

	public refresh( target_time: number ) {
		var diff: number = ( this.TargetDate.getTime() - target_time ) / 1000 << 0;
		var target = this.target;
		if ( target == null ) {
			//missing Div
			return;
		}
		if ( diff > 0 ) {
			var days: any = diff / 86400 << 0;
			var rest: any = diff - days * 86400;
			var hours: any = rest / 3600 << 0;
			rest -= hours * 3600;
			var minutes: any = rest / 60 << 0;
			rest -= minutes * 60;
			var seconds: any = rest << 0;
			days = this.format2( days );
			hours = this.format2( hours );
			minutes = this.format2( minutes );
			seconds = this.format2( seconds );
			var result = days + "</br>" + hours + "</br>" + minutes + "</br>" + seconds;
			target.innerHTML = result;
		} else {
			var result = Locale._t( "msg_reach" ) || Counter.MSG_REACH;
			var parent = target.parentElement;
			/*TODO: Is there a better solution? */
			var legend = parent.getElementsByClassName( "legend" ).item( 0 );
			if ( legend )
				parent.removeChild( legend );
			/* END TODO*/
			target.className = "rocket";
			target.innerHTML = result;
			this.refresh = ( e ) => {
			}
		}
	}

	public static toCounter( a: any ): Counter {
		if ( !( a instanceof Counter ) ) {
			var count = <Counter>a;
			return new Counter( count.TargetDate, count.DestName, count.target );
		}
	}

	public toObject(): Object {
		var o = { TargetDate: this.TargetDate, DestName: this.DestName, target: this.target };
		return ( o );
	}

	public showPopup(): void {
		this.popup.className = "popupVisible";
	}

	public hidePopup(): void {
		this.popup.className = "popupHidden";
	}

	private newPopup(): void {
		this.popup = document.createElement( 'div' );
		this.popup.className = "popupHidden";
		var result = document.createElement( 'span' );
		result.innerHTML = ( "<p>"
			+ ( Locale._t( "name" ) || "Name:" )
			+ "	"
			+ this.DestName + "</br>"
			+ ( Locale._t( "start" ) || "Start:" )
			+ "	"
			+ this.TargetDate.toLocaleDateString()
			+ "</br>"
			+ ( Locale._t( "at" ) || "at:")
			+ "		"
			+ this.TargetDate.toLocaleTimeString()
			+ " "
			+ ( Locale._t("clock") || "" ) + "</p>" );
		var buttonbox = document.createElement( 'div' );
		buttonbox.className = "buttonbox";
		var deleteButton = document.createElement( 'button' );
		var cancelButton = document.createElement( 'button' );
		cancelButton.innerHTML = Locale._t( "cancel" ) || "Cancel";
		deleteButton.className = "danger";
		deleteButton.innerHTML = Locale._t( "delete" ) || "Delete";
		deleteButton.addEventListener( 'click', this.remove.bind( this ) );
		cancelButton.addEventListener( 'click', this.hidePopup.bind( this ) );
		buttonbox.appendChild( cancelButton );
		buttonbox.appendChild( deleteButton );
		this.popup.appendChild( result );
		this.popup.appendChild( buttonbox );

	}

	public remove(): void {
		var remove: boolean = window.confirm( Locale._t( "conf_del" ) || Counter.CONF_DEL );
		if ( remove ) {
			app.deleteCounter( this );
		}
	}

	private format2( n: any ): string {
		if ( n > -1 && n < 10 ) {
			return "0" + n;
		}
		else {
			return <string>n;
		}
	}
}