var app:App; //MAIN APPLICATION CLASS
var transl :Locale;
function main() {
	transl = Locale.instance();
	start_app();
}


var start_app = function (){
	app = new App(transl);
	app.connectCallbacks();
	app.main();
}

class Exception {
static name :string = "Exception";
	constructor(public message:string){
	}
}

class InvalidInputException extends Exception {
static name: string = "InvalidInputException";
}

function check(assertion: boolean, message: string) {
	if (!assertion) {
		throw new InvalidInputException(message);
	}
}