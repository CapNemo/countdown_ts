var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/**
 * Checks if a given datestring is valid.
 * They should look like this:
 * YYYY-MM-DD-hh-mm			e.g. 2015-01-01-23-59
 */
var DateInvalidException = (function (_super) {
    __extends(DateInvalidException, _super);
    function DateInvalidException() {
        _super.apply(this, arguments);
    }
    DateInvalidException.name = "DateInvalidException";
    return DateInvalidException;
})(Exception);
var DateTime = (function () {
    function DateTime(dateString, timeString, hasTime) {
        this.setDateString(dateString, timeString, hasTime);
    }
    DateTime.prototype.setDateString = function (dateString, timeString, hasTime) {
        this.hasTime = hasTime;
        if (dateString == null || timeString == null)
            throw new DateInvalidException(DateTime.MSG_NO_DATE);
        var tmp = dateString + "-" + timeString.replace(':', '-');
        this.split = tmp.split("-");
        if (this.isValid()) {
            this.date = new Date(this.split[0], (this.split[1] - 1), this.split[2], this.split[3], this.split[4]);
        }
        else
            throw new DateInvalidException(DateTime.MSG_INVALID_DATE);
    };
    DateTime.prototype.isValid = function () {
        var valid = true;
        valid = (this.split.length == 5 && this.hasTime || this.split.length == 3 && !this.hasTime);
        console.log(valid);
        valid = valid && (this.split[0].length != 0 && this.split[1].length != 0 && this.split[2].length != 0);
        console.log(valid);
        var year = parseInt(this.split[0]);
        var month = parseInt(this.split[1]);
        var day = parseInt(this.split[2]);
        valid = valid && year > 1500 && year <= 9999;
        valid = valid && month <= 12 && month >= 1;
        valid = valid && day <= 31 && day >= 1;
        if (this.hasTime) {
            var hour = parseInt(this.split[3]);
            var minute = parseInt(this.split[4]);
            valid = valid && (hour <= 24 && hour >= 0);
            valid = valid && (minute <= 59 && minute >= 0);
        }
        return valid;
    };
    DateTime.prototype.getDate = function () {
        return this.date;
    };
    DateTime.MSG_NO_DATE = "Date or Time is empty";
    DateTime.MSG_INVALID_DATE = "Invalid Date";
    return DateTime;
})();
