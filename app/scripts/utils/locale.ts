class Locale {
	private static inst: Locale;
    private data: any;
    private lang: string;
    constructor() {
        this.lang = navigator.language || "en";
        var path: string = this.lang + ".js";
        this.loadLanguage( path );
    }

	public static instance(): Locale {
		if (Locale.inst) {
			return Locale.inst;
		} else
			Locale.inst = new Locale();
		return Locale.inst;
	}

	private set_data( data: any ) {
		this.data = data;
		this.translateByAttribute();
	}

	private translateByAttribute() {
		var nodes = document.querySelectorAll('[lang-prop]');
		for (var i = 0; i < nodes.length; ++i ) {
			var el = <HTMLElement>nodes[i];
			var prop = el.getAttribute("lang-prop");
			if (el.tagName === "INPUT" ) {
				if(this._t(prop))
					( <HTMLInputElement> el ).setAttribute("placeholder",this._t(prop));
			} else {
				if(this._t(prop))
					el.innerHTML = this._t(prop);
			}
		}
	}


    private loadLanguage( path: string ) {
		var script = ( <HTMLElement> document.createElement( "script" ) );
		script.setAttribute( 'type', "text/javascript" );
		script.setAttribute( 'src', path );
		document.getElementsByTagName('head')[0].appendChild( script );
	}

    /**
     * Returns the translated text to a given property in
     * the users language
     */
    public _t( prop: string ): string {
        if ( this.data ) {
			if ( this.data[prop] )
				return this.data[prop];
		}
        console.error( "Language: Property " + prop + " not found!" );
		return null;
    }
	
	public static _t(prop:string):string {
		return Locale.instance()._t(prop);
	}

}