/**
 * A DateTimeSelector is meant for use with FirefoxOS and is very similar
 * to an input of type 'datetime'.
 * 
 * It you don't run into problems when using the stock datetime input, then stick to it.
 * 
 * 
 */
interface DateTimeSelector extends HTMLInputElement {
}

/**
 * Checks if a given datestring is valid.
 * They should look like this:
 * YYYY-MM-DD-hh-mm			e.g. 2015-01-01-23-59
 */

class DateInvalidException extends Exception {
	static name = "DateInvalidException";
}

class DateTime {
	private static MSG_NO_DATE = "Date or Time is empty";
	private static MSG_INVALID_DATE = "Invalid Date";
	private split: Array<any>;
	private date: Date;
	private hasTime: boolean;
	constructor( dateString: string, timeString: string, hasTime: boolean ) {
		this.setDateString( dateString, timeString, hasTime );
	}


	public setDateString( dateString: string, timeString: string, hasTime: boolean ) {
		this.hasTime = hasTime;
		if ( dateString == null || timeString == null )
			throw new DateInvalidException( DateTime.MSG_NO_DATE );
		var tmp = dateString + "-" + timeString.replace( ':', '-' );
		this.split = tmp.split( "-" );
		if ( this.isValid() ) {
			this.date = new Date( this.split[0],( this.split[1] - 1 ), this.split[2], this.split[3], this.split[4] );
		}
		else
			throw new DateInvalidException( DateTime.MSG_INVALID_DATE );
	}


	public isValid(): boolean {
		var valid: boolean = true;
		valid = ( this.split.length == 5 && this.hasTime || this.split.length == 3 && !this.hasTime );
		console.log( valid );
		valid = valid && ( this.split[0].length != 0 && this.split[1].length != 0 && this.split[2].length != 0 )
		console.log( valid );
		var year: number = parseInt( this.split[0] );
		var month: number = parseInt( this.split[1] );
		var day: number = parseInt( this.split[2] );
		valid = valid && year > 1500 && year <= 9999;
		valid = valid && month <= 12 && month >= 1;
		valid = valid && day <= 31 && day >= 1;
		if ( this.hasTime ) {
			var hour = parseInt( this.split[3] );
			var minute = parseInt( this.split[4] );
			valid = valid && ( hour <= 24 && hour >= 0 );
			valid = valid && ( minute <= 59 && minute >= 0 );
		}
		return valid;
	}

	public getDate(): Date {
		return this.date;
	}


}