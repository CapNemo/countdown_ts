var Locale = (function () {
    function Locale() {
        this.lang = navigator.language || "en";
        var path = this.lang + ".js";
        this.loadLanguage(path);
    }
    Locale.instance = function () {
        if (Locale.inst) {
            return Locale.inst;
        }
        else
            Locale.inst = new Locale();
        return Locale.inst;
    };
    Locale.prototype.set_data = function (data) {
        this.data = data;
        this.translateByAttribute();
    };
    Locale.prototype.translateByAttribute = function () {
        var nodes = document.querySelectorAll('[lang-prop]');
        for (var i = 0; i < nodes.length; ++i) {
            var el = nodes[i];
            var prop = el.getAttribute("lang-prop");
            if (el.tagName === "INPUT") {
                if (this._t(prop))
                    el.setAttribute("placeholder", this._t(prop));
            }
            else {
                if (this._t(prop))
                    el.innerHTML = this._t(prop);
            }
        }
    };
    Locale.prototype.loadLanguage = function (path) {
        var script = document.createElement("script");
        script.setAttribute('type', "text/javascript");
        script.setAttribute('src', path);
        document.getElementsByTagName('head')[0].appendChild(script);
    };
    /**
     * Returns the translated text to a given property in
     * the users language
     */
    Locale.prototype._t = function (prop) {
        if (this.data) {
            if (this.data[prop])
                return this.data[prop];
        }
        console.error("Language: Property " + prop + " not found!");
        return null;
    };
    Locale._t = function (prop) {
        return Locale.instance()._t(prop);
    };
    return Locale;
})();
