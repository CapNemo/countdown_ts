class AsyncDatabaseException {
	static name: string = "asyncDatabaseException";
	constructor(public message: string) {
	}
}

class DBElement {
	constructor(public key: any, public value: any) {
	}
}

/**
 * AsyncDB is a class to simplify interaction
 * with IndexedDB.
 */
class AsyncDB {
	private database: IDBDatabase;
	private filledKeys: number = 0;
	private DB_NAME: string = "asyncDB";
	private STORE_NAME: string;
	constructor(dbName: string, storeName: string, callback_error?: () => void, callback_success?: () => void) {
		this.STORE_NAME = storeName.toUpperCase();
		this.DB_NAME = dbName;
		this.init(callback_error, callback_success);
	}



	/**
	 * Initialisiert die IndexedDB und ruft anschliessend callback_success auf
	 * oder callback_error im Falle eines Fehlers
	 * @param callback_error Funktionsreferenz fuer den Fehlerfall
	 * @param callback_success	Aufzurufende Funktion nach erfolgreicher Initialisierung
	 */
	private init(callback_error?: () => void, callback_success?: () => void) {
		var indexedDB = (<any>window).webkitIndexedDB || (<any>window).mozIndexedDB || window.indexedDB;
		if (!indexedDB){
			callback_error();
		}
		else {
			var request = indexedDB.open(this.DB_NAME, 21);
			request.onerror = (event: any) => {
				alert("Database Error " + request.error);
				if (callback_error) {
					alert(request.error);
					callback_error();
				}
			};

			request.onsuccess = () => {
				this.database = request.result;
				if (callback_success) {
					callback_success();
				}
			};

			request.onupgradeneeded = ( e: any ) => {
				this.database = request.result;
				var objstores = this.database.objectStoreNames;
				if ( objstores.contains( this.STORE_NAME )){
					//Nothing else to do;
				} else
					this.database.createObjectStore( this.STORE_NAME, { keyPath: "key" });
				console.log( "Store: " + this.STORE_NAME + " erstellt" );
			};
		}
	}

	public db_to_array(array: Array<any>, callback: () => void) {
		var trans = this.database.transaction(this.STORE_NAME, 'readonly');
		var store = trans.objectStore(this.STORE_NAME);
		var request = store.openCursor();
		var target = array;
		array.length = 0;
		request.onsuccess = (e: any) => {
			var result = request.result;
			if (result) {
				var data = (<DBElement>result.value).value;
				var key = (result).key;
				target.push(data);
				result.continue();
			} else {
				//ENDE DER DATENBANK
			};
		};
		trans.oncomplete = () => {
			if (callback) {
				callback();
			}
		};
	}

	public forEach(doFunction: (e: any) => void, callback: (e?: any) => void) {
		var trans = this.database.transaction(this.STORE_NAME, 'readonly');
		var store = trans.objectStore(this.STORE_NAME);
		var request = store.openCursor();
		request.onsuccess = (e: any) => {
			var result = request.result;
			if (result) {
				var data = (<DBElement>result.value).value;
				doFunction(data);
				result.continue();
			}
		}
		trans.oncomplete = callback;
	}


	public clear(callback?: () => void) {
		var trans = this.database.transaction(this.STORE_NAME, 'readwrite');
		var store = trans.objectStore(this.STORE_NAME);
		store.clear();
		trans.oncomplete = () => {
			if (callback) {
				callback();
			}
		}
	}

	public add(object: any, key: any, callback: () => void) {
		var trans = this.database.transaction(this.STORE_NAME, 'readwrite');
		var store = trans.objectStore(this.STORE_NAME);
		var data = new DBElement(key, object);
		var addRequest = store.add(data);
		addRequest.onsuccess = () => {
			this.filledKeys++;
		};

		addRequest.onerror = () => {
			console.error("Fehler beim Hinzufügen in die Datenbank");
		}
		trans.oncomplete = () => {
			if (callback) {
				callback();
			}
		}
	}

	public contains(key: any, callback: (e: boolean) => any) {
		var trans = this.database.transaction([this.STORE_NAME], 'readwrite');
		var store = trans.objectStore(this.STORE_NAME);
		var request = store.count(key);
		request.onsuccess = () => {
			callback(!(request.result === 0));
		}
	}

	public remove(key: any, callback: () => void) {
		if (!key) {
			console.error("key is undefined");
			return;
		}
		var trans = this.database.transaction([this.STORE_NAME], 'readwrite');
		var store = trans.objectStore(this.STORE_NAME);
		var request = (store.delete(key));
		trans.oncomplete = callback;
	}
}