var AsyncDatabaseException = (function () {
    function AsyncDatabaseException(message) {
        this.message = message;
    }
    AsyncDatabaseException.name = "asyncDatabaseException";
    return AsyncDatabaseException;
})();
var DBElement = (function () {
    function DBElement(key, value) {
        this.key = key;
        this.value = value;
    }
    return DBElement;
})();
/**
 * AsyncDB is a class to simplify interaction
 * with IndexedDB.
 */
var AsyncDB = (function () {
    function AsyncDB(dbName, storeName, callback_error, callback_success) {
        this.filledKeys = 0;
        this.DB_NAME = "asyncDB";
        this.STORE_NAME = storeName.toUpperCase();
        this.DB_NAME = dbName;
        this.init(callback_error, callback_success);
    }
    /**
     * Initialisiert die IndexedDB und ruft anschliessend callback_success auf
     * oder callback_error im Falle eines Fehlers
     * @param callback_error Funktionsreferenz fuer den Fehlerfall
     * @param callback_success	Aufzurufende Funktion nach erfolgreicher Initialisierung
     */
    AsyncDB.prototype.init = function (callback_error, callback_success) {
        var _this = this;
        var indexedDB = window.webkitIndexedDB || window.mozIndexedDB || window.indexedDB;
        if (!indexedDB) {
            callback_error();
        }
        else {
            var request = indexedDB.open(this.DB_NAME, 21);
            request.onerror = function (event) {
                alert("Database Error " + request.error);
                if (callback_error) {
                    alert(request.error);
                    callback_error();
                }
            };
            request.onsuccess = function () {
                _this.database = request.result;
                if (callback_success) {
                    callback_success();
                }
            };
            request.onupgradeneeded = function (e) {
                _this.database = request.result;
                var objstores = _this.database.objectStoreNames;
                if (objstores.contains(_this.STORE_NAME)) {
                }
                else
                    _this.database.createObjectStore(_this.STORE_NAME, { keyPath: "key" });
                console.log("Store: " + _this.STORE_NAME + " erstellt");
            };
        }
    };
    AsyncDB.prototype.db_to_array = function (array, callback) {
        var trans = this.database.transaction(this.STORE_NAME, 'readonly');
        var store = trans.objectStore(this.STORE_NAME);
        var request = store.openCursor();
        var target = array;
        array.length = 0;
        request.onsuccess = function (e) {
            var result = request.result;
            if (result) {
                var data = result.value.value;
                var key = (result).key;
                target.push(data);
                result.continue();
            }
            else {
            }
            ;
        };
        trans.oncomplete = function () {
            if (callback) {
                callback();
            }
        };
    };
    AsyncDB.prototype.forEach = function (doFunction, callback) {
        var trans = this.database.transaction(this.STORE_NAME, 'readonly');
        var store = trans.objectStore(this.STORE_NAME);
        var request = store.openCursor();
        request.onsuccess = function (e) {
            var result = request.result;
            if (result) {
                var data = result.value.value;
                doFunction(data);
                result.continue();
            }
        };
        trans.oncomplete = callback;
    };
    AsyncDB.prototype.clear = function (callback) {
        var trans = this.database.transaction(this.STORE_NAME, 'readwrite');
        var store = trans.objectStore(this.STORE_NAME);
        store.clear();
        trans.oncomplete = function () {
            if (callback) {
                callback();
            }
        };
    };
    AsyncDB.prototype.add = function (object, key, callback) {
        var _this = this;
        var trans = this.database.transaction(this.STORE_NAME, 'readwrite');
        var store = trans.objectStore(this.STORE_NAME);
        var data = new DBElement(key, object);
        var addRequest = store.add(data);
        addRequest.onsuccess = function () {
            _this.filledKeys++;
        };
        addRequest.onerror = function () {
            console.error("Fehler beim Hinzufügen in die Datenbank");
        };
        trans.oncomplete = function () {
            if (callback) {
                callback();
            }
        };
    };
    AsyncDB.prototype.contains = function (key, callback) {
        var trans = this.database.transaction([this.STORE_NAME], 'readwrite');
        var store = trans.objectStore(this.STORE_NAME);
        var request = store.count(key);
        request.onsuccess = function () {
            callback(!(request.result === 0));
        };
    };
    AsyncDB.prototype.remove = function (key, callback) {
        if (!key) {
            console.error("key is undefined");
            return;
        }
        var trans = this.database.transaction([this.STORE_NAME], 'readwrite');
        var store = trans.objectStore(this.STORE_NAME);
        var request = (store.delete(key));
        trans.oncomplete = callback;
    };
    return AsyncDB;
})();
