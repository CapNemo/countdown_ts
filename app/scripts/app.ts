class App {
	//Konstanten
	private static REFRESH_RATE: number = 1000; /* RefreshRate fuer die Counter */
	private static NAME = "CountdownApp";
	//Nachrichten
	private static MSG_DB_FAIL = "An Error occured while opening the database";
	private static MSG_NO_NAME = "Please enter a name";
	private static MSG_DATE_INVALID = "Invalid date";
	private static MSG_NOT_UNIQUE = "This name is already in use. Please choose another one";
	private static MSG_NO_DATE = "Please set a date first";
	private static GREETING = "It's empty here. </br>Create some countdowns";
	//Attribute
	private array: Counter[];
	private db: AsyncDB;
	// Lokalisierung
	private _t: ( prop: string ) => string;
	//UI-Elemente
	private body: HTMLElement;
	private header: HTMLElement;
	private button_back: HTMLElement;
	private button_add: HTMLElement;
	private content: HTMLElement;
	private editPage: HTMLElement;
	private newDate: HTMLInputElement;
	private newTime: HTMLInputElement;
	private newName: HTMLInputElement;
	private wholeDay: HTMLInputElement;
	private edit: boolean = false;
	private help1: string = "";
	private clearContent: boolean = false;
	private showDate: HTMLElement;

	constructor( locale: Locale ) {
		this.array = new Array<Counter>();
		this.db = new AsyncDB( App.NAME, "Counters",() => { alert( this._t( "db_fail" ) || App.MSG_DB_FAIL ); }, this.dbToArray.bind( this ) );
		var doc = document;
		this.body = doc.body;
		this.header = doc.getElementById( "header" );
		this.button_back = <HTMLElement>this.header.querySelector( "#button_back" );
		this.button_add = <HTMLElement>this.header.querySelector( "#button_add" );
		this.content = doc.getElementById( "content" );
		this.editPage = doc.getElementById( "editPage" );
		this.newDate = <HTMLInputElement>doc.getElementById( "newDate" );
		this.newTime = <HTMLInputElement>doc.getElementById( "newTime" );
		this.newName = <HTMLInputElement>doc.getElementById("newName");
		this.wholeDay = <HTMLInputElement>doc.getElementById("wholeDay");
		this.showDate = <HTMLElement> doc.getElementById( "showDate" );
		if ( locale ) {
			this._t = locale._t.bind( locale );
		} else {
			this._t = () => { console.error( "Localization not possible!" ); return null; };
		}
	}

	public main(): void {
		window.setInterval( this.refreshCounters.bind( this ), App.REFRESH_RATE );
	}

	private showSelectedDate( date: Date ): void {
		this.showDate.innerHTML = date.toLocaleString();
	}

	public connectCallbacks(): void {
		this.button_back.addEventListener( "click",() => {
			this.edit = false;
			this.showMain();
			this.button_back.style.display = "none";
			this.button_add.innerHTML = this.help1;
			this.button_add.style.display = "inline";
		});
		this.button_add.addEventListener( "click",() => {
			if ( this.edit ) {
				try {
					this.createNewCounter();
					this.edit = false;
					this.button_add.innerHTML = this.help1;
					this.button_back.style.display = "none";
				} catch ( e ) {
					alert( e.message );
				}
			} else {
				this.help1 = this.button_add.innerHTML;
				this.showAddPage();
				this.button_add.innerHTML = "OK";
				this.button_back.style.display = "inline";
				this.edit = true;
			}
		});
		this.wholeDay.onchange = (e) => {
			if (this.wholeDay.checked) {
				this.newTime.disabled = true;
				this.newTime.classList.add( "disabled");
			} else {
				this.newTime.disabled = false;
				this.newTime.classList.remove("disabled");
			}
		};
		
	}

	private refreshCounters() {
		var date = Date.now();
		var length = this.array.length;
		for ( var i = 0; i < length; i++ ) {
			this.array[i].refresh( date );
		}
	}

	/**
	* Liest alle Counter aus der Datenbank in das Array
	*/
	private dbToArray() {
		this.array.length = 0;
		this.db.forEach(( el ) => { this.array.push( Counter.toCounter( el ) ); }, this.buildUI.bind( this ) );
	}

	public deleteCounter( counter: Counter ) {
		this.db.remove( counter.DestName, this.dbToArray.bind( this ) );
		var parent = counter.target.parentElement;
		var popup = counter.popup;
		parent.parentElement.removeChild( parent );
		popup.parentElement.removeChild( popup );
	}

	private updateDB( newValue: any ) {
		this.db.add( newValue, newValue.DestName, this.buildUI.bind( this ) );
	}

	private addCounter( TargetDate: Date, DestName: string ) {
		check( this.array != null, "TargetArray does not exist!" );
		this.db.contains( DestName,( b: boolean ) => {
			if ( !b ) {
				this.array.push( new Counter( TargetDate, DestName ) );
				this.updateDB( new Counter( TargetDate, DestName ).toObject() );
			} else
				alert( this._t( "msg_not_unique" ) || App.MSG_NOT_UNIQUE );
		});
	}

	private createNewCounter() {
		var new_date: string = this.newDate.value;
		var new_time: string = this.newTime.value || "00:00"; //DEFAULT TO 0:00
		var name: string = this.newName.value;
		check( name != null && name.length > 0, this._t( "no_name" ) || App.MSG_NO_NAME );
		check( new_date != null, this._t( "no_date" ) || App.MSG_NO_DATE );
		/* TODO:
		if(! this.full_day.enabled){
			check(new_time != null, this._t("no_time")|| App.MSG_NO_TIME);
		}
		*/
		var datetime = new DateTime( new_date, new_time, true );
		var date = datetime.getDate();
		this.addCounter( date, name );
		this.showMain();
	}

	private firstStart() {
		var title = document.createElement( 'h1' );
		title.innerHTML = this._t( "greet" ) || App.GREETING;
		this.content.appendChild( title );
		this.clearContent = true;
	}

	private buildUI() {
		var doc = document;
		if ( this.array.length == 0 ) {
			this.firstStart();
		} else {
			if ( this.clearContent ) {
				this.content.innerHTML = "";
				this.clearContent = false;
			}
			var fragment = doc.createDocumentFragment();
			for ( var i = 0; i < this.array.length; i++ ) {
				var el = this.array[i];
				if ( !this.exists( el.DestName ) ) {
					fragment.appendChild( this.buildDiv( el ) );
					fragment.appendChild( el.popup );
					el.refresh( Date.now() );
				} else if ( !el.target ) {
					el.target = doc.getElementById( el.DestName );
				}
			}
			this.content.appendChild( fragment );
		}
	}

	private buildDiv( c: Counter ): HTMLElement {
		var doc = document;
		var title = doc.createElement( 'h2' );
		title.innerHTML = c.DestName;
		var div = doc.createElement( 'div' );
		div.onclick = c.showPopup.bind( c );
		div.appendChild( title );
		var p = doc.createElement( 'div' );
		p.className = "count_target";
		p.id = c.DestName;
		c.target = p;
		var legend = doc.createElement( 'div' );
		legend.className = "legend";
		legend.innerHTML = this._t( "legend" ) || "Days</br>Hours</br>Minutes</br>Seconds";
		div.appendChild( legend );
		div.appendChild( p );
		return div;
	}

	private exists( id: string ): boolean {
		return ( !( !document.getElementById( id ) ) );
	}
	/* Verbirgt die Countdowns und zeigt die Seite zum erstellen neuer */
	private showAddPage() {
		this.content.style.display = 'none';
		this.editPage.style.display = 'block';
	}
	/* Kehrt zur Hauptseite zurueck */
	private showMain() {
		this.content.style.display = 'block';
		this.editPage.style.display = 'none';
	}
}