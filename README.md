# Countdown #
-------------------------------------------------------------
## Deutsch: ##
Einfacher Countdown für FirefoxOS, geschrieben in Typescript, CSS und HTML5.
FÜr CSS wurde auch BuildingBlocks [http://buildingfirefoxos.com/](http://buildingfirefoxos.com/) verwendet.

**Sprachen:**  
Deutsch  
TODO: Englisch
-------------------------------------------------------------
##Englisch: ##
Very simple countdown for FirefoxOS
  
**Languages:**  
German  
TODO: English

* If you have questions or suggestions please do not hestitate to contact me.